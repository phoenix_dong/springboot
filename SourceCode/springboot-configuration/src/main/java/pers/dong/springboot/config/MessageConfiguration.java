package pers.dong.springboot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: dong
 * @Project: springboot
 * @Description:
 * @Date: Created in 1:02 PM 2018/11/10
 * @Modified By:
 */

@Configuration
public class MessageConfiguration {
    @Bean
    public String message() {
        return "message configuration";
    }
}
