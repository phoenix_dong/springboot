package pers.dong.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pers.dong.springboot.config.MessageConfiguration;


/**
 * @Author: dong
 * @Project: springboot
 * @Description: Spring Boot 应用启动类
 * @Date: Created in 1:01 PM 2018/11/10
 * @Modified By:
 */

// Spring Boot 应用的标识
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        // 程序启动入口
        // 启动嵌入式的 Tomcat 并初始化 Spring 环境及其各 Spring 组件
        //SpringApplication.run(Application.class, args);


        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();

        context.register(MessageConfiguration.class);
        context.refresh();

        String mybean = (String)context.getBean("message");
        System.out.print(mybean);
    }
}
