package pers.dong.springboot.reflection;

/**
 * @Author: dong
 * @Project: springboot
 * @Description:
 * @Date: Created in 15:37 2019-01-03
 * @Modified By:
 */
public class ReflectTest {
    public static void main(String[] args) {
        System.out.println(XYZ.name);
    }
}

class XYZ {
    public static String name = "luoxn28";

    static {
        System.out.println("xyz静态块");
    }

    public XYZ() {
        System.out.println("xyz构造了");
    }
}
