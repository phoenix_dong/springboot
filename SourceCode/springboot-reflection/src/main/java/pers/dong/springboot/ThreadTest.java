package pers.dong.springboot;

/**
 * @Author: dong
 * @Project: springboot
 * @Description:
 * @Date: Created in 13:09 2019-01-09
 * @Modified By:
 */
public class ThreadTest {
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            System.out.println(Thread.currentThread().getName() + " " + i);
            if (i == 7) {
                Runnable myRunnable = new MyRunnable();
                Thread thread1 = new MyThread(myRunnable);
                Thread thread2 = new MyThread(myRunnable);
                Thread thread3 = new MyThread(myRunnable);
                thread1.start();
                thread2.start();
                thread3.start();
            }
        }
    }
}


class MyRunnable implements Runnable {
    private int i = 0;
    @Override
    public void run() {
        System.out.println("in MyRunnable run");
        for (i = 0; i < 10; i++) {
            System.out.println("Runnable" + Thread.currentThread().getName() + " " + i);
        }
    }
}

class MyThread extends Thread {
    private int i = 0;
    public MyThread(Runnable runnable){
        super(runnable);
    }

    @Override
    public void run() {
        System.out.println("in MyThread run");
        for (i = 0; i < 10; i++) {
            System.out.println("Thread" + Thread.currentThread().getName() + " " + i);
        }
    }
}