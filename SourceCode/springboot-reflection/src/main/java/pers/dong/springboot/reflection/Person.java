package pers.dong.springboot.reflection;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @Author: dong
 * @Project: springboot
 * @Description:
 * @Date: Created in 15:49 2019-01-03
 * @Modified By:
 */
public class Person implements Serializable {
    private String name;
    private int age;

    public String getName() {
        System.out.println("getName()");
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        System.out.println("getAge()");
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Person(String name, int age) {
        System.out.println("Person 构造方法");
        this.name = name;
        this.age = age;
    }
}

class test {
    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException, IntrospectionException {
        Person person = new Person("luoxn28", 23);
        Class clazz = person.getClass();

        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            String key = field.getName();
            PropertyDescriptor descriptor = new PropertyDescriptor(key, clazz);
            Method method = descriptor.getReadMethod();
            Object value = method.invoke(person);

            System.out.println(key + ":" + value);

        }
    }
}

