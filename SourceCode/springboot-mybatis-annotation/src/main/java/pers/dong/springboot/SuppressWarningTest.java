package pers.dong.springboot;

import java.util.Date;

/**
 * @Author: dong
 * @Project: springboot
 * @Description:
 * @Date: Created in 13:59 2018-12-25
 * @Modified By:
 */
public class SuppressWarningTest {
    //@SuppressWarnings(value={"deprecation"})
    public static void doSomething(){
        Date date = new Date(113, 8, 26);
        System.out.println(date);
    }

    public static void main(String[] args) {
        doSomething();
    }
}
