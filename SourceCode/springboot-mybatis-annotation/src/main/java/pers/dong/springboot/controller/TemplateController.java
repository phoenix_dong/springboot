package pers.dong.springboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class TemplateController {

    @GetMapping("index")
    public String index(ModelMap map){
        map.put("welcome","SpringBoot Hello World！");
        return "index";
    }
}
