package pers.dong.springboot.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pers.dong.springboot.dao.CityDao;
import pers.dong.springboot.entity.City;
import pers.dong.springboot.service.CityService;

import java.util.List;

/**
 * @Author: dong
 * @Project: springboot
 * @Description:
 * @Date: Created in 2:04 PM 2018/11/10
 * @Modified By:
 */
@Service
public class CityServiceImpl implements CityService {

    @Autowired
    private CityDao cityDao;

    @Override
    public List<City> findAllCity(){
        return cityDao.findAllCity();
    }

    @Override
    public City findCityById(Long id) {
        return cityDao.findById(id);
    }

    @Override
    public Long saveCity(City city) {
        return cityDao.saveCity(city);
    }

    @Override
    public Long updateCity(City city) {
        return cityDao.updateCity(city);
    }

    @Override
    public Long deleteCity(Long id) {
        return cityDao.deleteCity(id);
    }

}
