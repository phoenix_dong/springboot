package pers.dong.springboot.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import pers.dong.springboot.entity.City;
import pers.dong.springboot.service.CityService;

import java.util.List;

/**
 * @Author: dong
 * @Project: springboot
 * @Description: 城市 Controller 实现 Restful HTTP 服务
 * @Date: Created in 1:51 PM 2018/11/10
 * @Modified By:
 */
@Api(tags = "1.0", description = "swaggerDemoController相关的api", value = "城市信息管理")
@RestController
public class CityRestController {

    @Autowired
    private CityService cityService;

    @ApiOperation(value = "查询一个城市1")
    @RequestMapping(value = "/api/city/{id}", method = RequestMethod.GET)
    public City findOneCity(@PathVariable("id") Long id) {
        return cityService.findCityById(id);
    }

    @ApiOperation(value = "查询多个城市")
    @RequestMapping(value = "/api/city", method = RequestMethod.GET)
    public List<City> findAllCity() {
        return cityService.findAllCity();
    }

    @ApiOperation(value = "增加城市信息1")
    @RequestMapping(value = "/api/city", method = RequestMethod.POST)
    public void createCity(@RequestBody City city) {
        cityService.saveCity(city);
    }

    @ApiOperation(value = "更新城市信息")
    @RequestMapping(value = "/api/city", method = RequestMethod.PUT)
    public void modifyCity(@RequestBody City city) {
        cityService.updateCity(city);
    }

    @ApiOperation(value = "删除城市信息")
    @RequestMapping(value = "/api/city/{id}", method = RequestMethod.DELETE)
    public void modifyCity(@PathVariable("id") Long id) {
        cityService.deleteCity(id);
    }


    @ApiOperation(value = "增加城市信息2")
    @RequestMapping(value = "/api/city2", method = RequestMethod.POST, consumes="application/json")
    public void addCity(@RequestBody City city) {
        cityService.saveCity(city);
    }

    @ApiOperation(value = "查询一个城市2")
    @RequestMapping(value = "/api/city/{id}", method = RequestMethod.GET, produces="application/json")
    public City findOneCity2(@PathVariable("id") Long id) {
        return cityService.findCityById(id);
    }

    @ApiOperation(value = "查询一个城市3")
    @RequestMapping(value = "/api/city/{id}", method = RequestMethod.GET, params="myParam=myValue")
    public City findOneCity3(@PathVariable("id") Long id) {
        return cityService.findCityById(id);
    }

    @ApiOperation(value = "查询一个城市4")
    @RequestMapping(value = "/api/city/{id}", method = RequestMethod.GET, headers="Referer=http://127.0.0.1/")
    public City findOneCity4(@PathVariable("id") Long id) {
        return cityService.findCityById(id);
    }


}
