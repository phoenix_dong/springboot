package pers.dong.springboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;

/**
 * @Author: dong
 * @Project: springboot
 * @Description: Spring Boot 应用启动类
 * @Date: Created in 3:30 PM 2018/11/16
 * @Modified By:
 */
@MapperScan("pers.dong.springboot.dao")
public class Application {
    public static void main(String[] args) {
        // 程序启动入口
        // 启动嵌入式的 Tomcat 并初始化 Spring 环境及其各 Spring 组件
        SpringApplication.run(Application.class,args);
    }
}
