package pers.dong.springboot.dao;

import org.apache.ibatis.annotations.Param;
import pers.dong.springboot.entity.City;

/**
 * @Author: dong
 * @Project: springboot
 * @Description: 城市 DAO 接口类
 * @Date: Created in 2:46 PM 2018/11/25
 * @Modified By:
 */
public interface CityDao {
    /**
     * 根据城市名称，查询城市信息
     *
     * @param cityName 城市名
     */
    City findCityByName(@Param("cityName") String cityName);


    /**
     * 插入城市信息
     * @param city
     */
    void insertCity(City city);

    /**
     * 插入城市信息
     * @param city
     */
    void updateCity(City city);

    /**
     * 插入城市信息方法2
     * @param city
     */
    void updateCity1(City city);
}
