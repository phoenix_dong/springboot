package pers.dong.springboot.service;

import pers.dong.springboot.entity.City;

/**
 * @Author: dong
 * @Project: springboot
 * @Description: 城市业务逻辑接口类
 * @Date: Created in 2:42 PM 2018/11/25
 * @Modified By:
 */
public interface CityService {
    /**
     * 根据城市名称查询城市信息
     * @param name
     * @return
     */
    City findCityByName(String name);
}
