package pers.dong.springboot.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import pers.dong.springboot.dao.CityDao;
import pers.dong.springboot.entity.City;
import pers.dong.springboot.service.CityService;

/**
 * @Author: dong
 * @Project: springboot
 * @Description:
 * @Date: Created in 2:48 PM 2018/11/25
 * @Modified By:
 */
public class CityServiceImpl implements CityService {
    @Autowired
    private CityDao cityDao;

    @Override
    public City findCityByName(String name) {
        return cityDao.findCityByName(name);
    }
}
