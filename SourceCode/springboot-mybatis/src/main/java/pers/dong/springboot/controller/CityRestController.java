package pers.dong.springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pers.dong.springboot.entity.City;
import pers.dong.springboot.service.CityService;

/**
 * @Author: dong
 * @Project: springboot
 * @Description: 城市 Controller 实现 Restful HTTP 服务
 * @Date: Created in 2:41 PM 2018/11/25
 * @Modified By:
 */
@RestController
public class CityRestController {
    @Autowired
    private CityService cityService;

    @RequestMapping(value = "/api/city", method = RequestMethod.GET)
    public City findOneCity(@RequestParam(value = "cityName", required = true) String cityName) {
        return cityService.findCityByName(cityName);
    }
}
