package pers.dong.springboot.dao.mysql1;

import org.apache.ibatis.annotations.Param;
import pers.dong.springboot.entity.UserInfo;

/**
 * @Author: dong
 * @Project: springboot
 * @Description:
 * @Date: Created in 16:12 2018-12-26
 * @Modified By:
 */
public interface UserInfoDao {
    /**
     * 根据用户名获取用户信息
     * @param userName
     * @return 用户信息
     */
    UserInfo findUserInfo(@Param("userName") String userName);
}
