package pers.dong.springboot.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pers.dong.springboot.dao.mysql1.UserInfoDao;
import pers.dong.springboot.dao.mysql2.CityInfoDao;
import pers.dong.springboot.entity.UserInfo;
import pers.dong.springboot.service.UserInfoService;

/**
 * @Author: dong
 * @Project: springboot
 * @Description:
 * @Date: Created in 15:44 2018-12-26
 * @Modified By:
 */
@Service("userInfoService")
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    @Qualifier("userInfoDao")
    private UserInfoDao userInfoDao;
    @Autowired
    @Qualifier("cityInfoDao")
    private CityInfoDao cityInfoDao;

    @Override
    public UserInfo findUserInfo(String userName) {
        try{
            UserInfo userInfo = userInfoDao.findUserInfo(userName);
            String cityName = cityInfoDao.findCityName(userInfo.getCityId());
            userInfo.setCityInfo(cityName);
            return userInfo;
        }
        catch (Exception e) {
            return null;
        }

    }
}
