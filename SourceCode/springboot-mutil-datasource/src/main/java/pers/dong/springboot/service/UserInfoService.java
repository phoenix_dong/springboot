package pers.dong.springboot.service;


import pers.dong.springboot.entity.UserInfo;

/**
 * @Author: dong
 * @Project: springboot
 * @Description:
 * @Date: Created in 15:44 2018-12-26
 * @Modified By:
 */
public interface UserInfoService {

    /**
     * 根据用户名获取用户信息
     * @param userName
     * @return 用户信息
     */
    UserInfo findUserInfo(String userName);
}
