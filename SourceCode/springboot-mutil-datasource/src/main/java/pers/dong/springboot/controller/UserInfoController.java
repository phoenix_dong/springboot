package pers.dong.springboot.controller;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pers.dong.springboot.entity.UserInfo;
import pers.dong.springboot.service.UserInfoService;

/**
 * @Author: dong
 * @Project: springboot
 * @Description:
 * @Date: Created in 14:15 2018-12-26
 * @Modified By:
 */
@Api(tags = "1.0", description = "用户信息Controller相关的api", value = "用户信息查询")
@RestController
public class UserInfoController {

    @Autowired
    private UserInfoService userInfoService;

    /**
     * 根据用户名获取用户信息
     * @param userName
     * @return
     */
    @RequestMapping(value = "/api/user", method = RequestMethod.GET)
    public UserInfo findUserInfo(@RequestParam(value = "userName", required = true) String userName) {
        return userInfoService.findUserInfo(userName);
    }

}
