package pers.dong.springboot.config.mysql;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

/**
 * @Author: dong
 * @Project: springboot
 * @Description: 第一个Mysql数据源配置类
 * @Date: Created in 13:25 2018-12-26
 * @Modified By:
 */
@Configuration
// 扫描 Mapper 接口并容器管理
@MapperScan(basePackages = Mysql1DataSourceConfig.PACKAGE, sqlSessionFactoryRef = "mysql1SqlSessionFactory")
public class Mysql1DataSourceConfig {
    // 精确到 mysql1 目录，以便跟其他数据源隔离
    static final String PACKAGE = "pers.dong.springboot.dao.mysql1";
    static final String MAPPER_LOCATION = "classpath:mapper/mysql1/*.xml";

    @Value("${mysql1.datasource.url}")
    private String url;

    @Value("${mysql1.datasource.username}")
    private String user;

    @Value("${mysql1.datasource.password}")
    private String password;

    @Value("${mysql1.datasource.driver-class-name}")
    private String driverClass;

    @Bean(name = "mysql1DataSource")
    @Primary
    public DataSource mysql1DataSource() {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName(driverClass);
        dataSource.setUrl(url);
        dataSource.setUsername(user);
        dataSource.setPassword(password);
        return dataSource;
    }

    @Bean(name = "mysql1TransactionManager")
    @Primary
    public DataSourceTransactionManager mysql1TransactionManager() {
        return new DataSourceTransactionManager(mysql1DataSource());
    }

    @Bean(name = "mysql1SqlSessionFactory")
    @Primary
    public SqlSessionFactory mysql1SqlSessionFactory(@Qualifier("mysql1DataSource") DataSource mysql1DataSource)
            throws Exception {
        final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(mysql1DataSource);
        sessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver()
                .getResources(Mysql1DataSourceConfig.MAPPER_LOCATION));
        return sessionFactory.getObject();
    }

}
