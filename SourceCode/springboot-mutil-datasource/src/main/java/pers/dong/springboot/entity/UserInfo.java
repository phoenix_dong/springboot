package pers.dong.springboot.entity;

/**
 * @Author: dong
 * @Project: springboot
 * @Description: 用户实体类
 * @Date: Created in 15:46 2018-12-26
 * @Modified By:
 */
public class UserInfo {
    //用户id
    private String id;

    //用户姓名
    private String userName;

    //用户账号
    private String account;

    //年龄
    private int age;

    //city id
    private String cityId;

    //city info
    private String cityInfo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityInfo() {
        return cityInfo;
    }

    public void setCityInfo(String cityInfo) {
        this.cityInfo = cityInfo;
    }
}
