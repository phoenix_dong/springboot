package pers.dong.springboot.config.mysql;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

/**
 * @Author: dong
 * @Project: springboot
 * @Description: 第一个Mysql数据源配置类
 * @Date: Created in 13:26 2018-12-26
 * @Modified By:
 */
@Configuration
// 扫描 Mapper 接口并容器管理
@MapperScan(basePackages = Mysql2DataSourceConfig.PACKAGE, sqlSessionFactoryRef = "mysql2SqlSessionFactory")
public class Mysql2DataSourceConfig {
    // 精确到 mysql2 目录，以便跟其他数据源隔离
    static final String PACKAGE = "pers.dong.springboot.dao.mysql2";
    static final String MAPPER_LOCATION = "classpath:mapper/mysql2/*.xml";

    @Value("${mysql2.datasource.url}")
    private String url;

    @Value("${mysql2.datasource.username}")
    private String user;

    @Value("${mysql2.datasource.password}")
    private String password;

    @Value("${mysql2.datasource.driver-class-name}")
    private String driverClass;

    @Bean(name = "mysql2DataSource")
    public DataSource mysql2DataSource() {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName(driverClass);
        dataSource.setUrl(url);
        dataSource.setUsername(user);
        dataSource.setPassword(password);
        return dataSource;
    }

    @Bean(name = "mysql2TransactionManager")
    public DataSourceTransactionManager mysql2TransactionManager() {
        return new DataSourceTransactionManager(mysql2DataSource());
    }

    @Bean(name = "mysql2SqlSessionFactory")
    public SqlSessionFactory mysql2SqlSessionFactory(@Qualifier("mysql2DataSource") DataSource mysql2DataSource)
            throws Exception {
        final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(mysql2DataSource);
        sessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver()
                .getResources(Mysql2DataSourceConfig.MAPPER_LOCATION));
        return sessionFactory.getObject();
    }

}
