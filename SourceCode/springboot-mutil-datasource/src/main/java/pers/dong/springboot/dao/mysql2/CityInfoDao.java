package pers.dong.springboot.dao.mysql2;

import org.apache.ibatis.annotations.Param;

/**
 * @Author: dong
 * @Project: springboot
 * @Description:
 * @Date: Created in 16:16 2018-12-26
 * @Modified By:
 */
public interface CityInfoDao {

    /**
     * 根据城市id获取城市名称
     * @param cityId
     * @return
     */
    String findCityName(@Param("cityId") String cityId);
}
