package pers.dong.springboot.config;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @Author: dong
 * @Project: springboot
 * @Description:
 * @Date: Created in 10:18 AM 2018/11/10
 * @Modified By:
 */
public class HelloWorldControllerTest {

    @Test
    public void sayHello() {
        /*
         * Assert.assertEquals();及其重载方法:
         * 1. 如果两者一致, 程序继续往下运行.
         * 2. 如果两者不一致, 中断测试方法, 抛出异常信息 AssertionFailedError .
         */
        assertEquals("Hello,World!",new HelloWorldController().sayHello());
    }
}