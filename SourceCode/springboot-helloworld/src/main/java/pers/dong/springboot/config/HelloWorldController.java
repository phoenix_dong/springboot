package pers.dong.springboot.config;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: dong
 * @Project: springboot-helloworld
 * @Description:
 * @Date: Created in 9:34 AM 2018/11/7
 * @Modified By:
 */
@RestController
public class HelloWorldController {

    @RequestMapping("/helloworld")
    public String sayHello () {
        return "Hello World!";
    }

}
