package pers.dong.springboot;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: dong
 * @Project: springboot-helloworld
 * @Description:
 * @Date: Created in 2:10 PM 2018/11/9
 * @Modified By:
 */

//Spring Boot 应用的标示
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        // 程序启动入口
        // 启动嵌入式的 Tomcat 并初始化 Spring 环境及其各 Spring 组件
        SpringApplication.run(Application.class, args);
    }
}
