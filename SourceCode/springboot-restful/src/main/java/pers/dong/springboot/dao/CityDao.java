package pers.dong.springboot.dao;

import org.apache.ibatis.annotations.Param;
import pers.dong.springboot.entity.City;

import java.util.List;

/**
 * @Author: dong
 * @Project: springboot
 * @Description:
 * @Date: Created in 2:05 PM 2018/11/10
 * @Modified By:
 */
public interface CityDao {

    /**
     * 获取城市信息列表
     *
     * @return
     */
    List<City> findAllCity();

    /**
     * 根据城市 ID，获取城市信息
     *
     * @param id
     * @return
     */
    City findById(@Param("id") Long id);

    Long saveCity(City city);

    Long updateCity(City city);

    Long deleteCity(Long id);
}
