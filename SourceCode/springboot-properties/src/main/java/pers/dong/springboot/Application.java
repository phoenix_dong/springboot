package pers.dong.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pers.dong.springboot.property.HomeProperties;

/**
 * @Author: dong
 * @Project: springboot-examples
 * @Description:
 * @Date: Created in 3:05 PM 2018/11/9
 * @Modified By:
 */
// Spring Boot 应用的标识
//通过实现接口 CommandLineRunner 来实现:
@SpringBootApplication
public class Application implements CommandLineRunner {

    @Autowired
    private HomeProperties homeProperties;

    public static void main(String[] args) {
        // 程序启动入口
        // 启动嵌入式的 Tomcat 并初始化 Spring 环境及其各 Spring 组件
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run (String... args) throws Exception {
        System.out.println("\n" + homeProperties.toString());
        System.out.println();
    }

}
