package pers.dong.springboot.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author: dong
 * @Project: springboot
 * @Description:
 * @Date: Created in 10:24 AM 2018/11/10
 * @Modified By:
 */
@Component
@ConfigurationProperties(prefix = "user")
public class UserProperties {

    /**
     * 用户 ID
     */
    private Long id;

    /**
     * 年龄
     */
    private int age;

    /**
     * 用户名称
     */
    private String desc;

    /**
     * 用户 UUID
     */
    private String uuid;

    public Long getId() {
        return id;
    }

    public int getAge() {
        return age;
    }

    public String getDesc() {
        return desc;
    }

    public String getUuid() {
        return uuid;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public String toString() {
        return "HomeProperties{" +
                "id=" + id +
                ", age=" + age +
                ", desc='" + desc + '\'' +
                ", uuid='" + uuid + '\'' +
                '}';
    }
}
