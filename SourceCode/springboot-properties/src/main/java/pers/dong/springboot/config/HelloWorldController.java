package pers.dong.springboot.config;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: dong
 * @Project: springboot
 * @Description:
 * @Date: Created in 10:29 AM 2018/11/10
 * @Modified By:
 */

@RestController
public class HelloWorldController {
    @RequestMapping("/")
    public String sayHello () {
        return "Hello World!";
    }

}
