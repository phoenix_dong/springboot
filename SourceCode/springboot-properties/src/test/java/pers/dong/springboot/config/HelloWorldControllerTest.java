package pers.dong.springboot.config;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @Author: dong
 * @Project: springboot
 * @Description:
 * @Date: Created in 11:41 AM 2018/11/10
 * @Modified By:
 */
public class HelloWorldControllerTest {

    @Test
    public void sayHello() {
        assertEquals("Hello,Wrold", new HelloWorldController().sayHello());
    }
}