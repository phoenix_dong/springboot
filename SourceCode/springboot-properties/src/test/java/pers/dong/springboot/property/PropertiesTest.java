package pers.dong.springboot.property;

import org.junit.Test;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.slf4j.Logger;


/**
 * @Author: dong
 * @Project: springboot
 * @Description: 自定义配置文件配置类
 * @Date: Created in 11:49 AM 2018/11/10
 * @Modified By:
 */
public class PropertiesTest {

    @Autowired
    private HomeProperties homeProperties;

    @Autowired
    private UserProperties userProperties;

    private static final Logger LOGGER = LoggerFactory.getLogger(PropertiesTest.class);

    @Test
    public void getHomeProperties() {
        LOGGER.info("\n\n" + homeProperties.toString() + "\n");
    }

    @Test
    public void getUserProperties() {
        LOGGER.info("\n\n" + userProperties.toString() + "\n");
    }
}
