package pers.dong.springboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Author: dong
 * @Project: springboot
 * @Description: Spring Boot 应用启动类
 * @Date: Created in 5:25 PM 2018/11/12
 * @Modified By:
 */
// Spring Boot 应用的标识
@SpringBootApplication
@MapperScan("pers.dong.springboot.dao")
public class Application {
    public static void main(String[] args) {
        // 程序启动入口
        // 启动嵌入式的 Tomcat 并初始化 Spring 环境及其各 Spring 组件
        SpringApplication.run(Application.class, args);
    }
}
