package pers.dong.springboot.service;

import pers.dong.springboot.entity.City;

import java.util.List;

/**
 * @Author: dong
 * @Project: springboot
 * @Description: 城市业务逻辑接口类
 * @Date: Created in 2:00 PM 2018/11/10
 * @Modified By:
 */
public interface CityService {

    /**
     * 获取城市信息列表
     *
     * @return
     */
    List<City> findAllCity();

    /**
     * 根据城市 ID,查询城市信息
     *
     * @param id
     * @return
     */
    City findCityById(Long id);

    /**
     * 新增城市信息
     *
     * @param city
     * @return
     */
    Long saveCity(City city);

    /**
     * 更新城市信息
     *
     * @param city
     * @return
     */
    Long updateCity(City city);

    /**
     * 根据城市 ID,删除城市信息
     *
     * @param id
     * @return
     */
    Long deleteCity(Long id);
}
