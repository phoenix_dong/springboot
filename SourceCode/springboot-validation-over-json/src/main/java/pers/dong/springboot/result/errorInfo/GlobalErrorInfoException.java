package pers.dong.springboot.result.errorInfo;

import pers.dong.springboot.result.ErrorInfoInterface;

/**
 * @Author: dong
 * @Project: springboot
 * @Description: 统一错误码异常
 * @Date: Created in 9:11 AM 2018/11/13
 * @Modified By:
 */
public class GlobalErrorInfoException extends Exception {
    private ErrorInfoInterface errorInfo;

    public GlobalErrorInfoException (ErrorInfoInterface errorInfo) {
        this.errorInfo = errorInfo;
    }

    public ErrorInfoInterface getErrorInfo() {
        return errorInfo;
    }

    public void setErrorInfo(ErrorInfoInterface errorInfo) {
        this.errorInfo = errorInfo;
    }

}