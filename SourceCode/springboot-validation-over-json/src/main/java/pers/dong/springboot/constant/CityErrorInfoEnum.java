package pers.dong.springboot.constant;

import pers.dong.springboot.result.ErrorInfoInterface;

/**
 * @Author: dong
 * @Project: springboot
 * @Description: 业务错误码 案例
 * @Date: Created in 9:20 AM 2018/11/13
 * @Modified By:
 */
public enum CityErrorInfoEnum implements ErrorInfoInterface {
    PARAMS_NO_COMPLETE("000001","params no complete"),
    CITY_EXIT("000002","city exit");

    private String code;

    private String message;

    CityErrorInfoEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getCode(){
        return this.code;
    }

    @Override
    public String getMessage(){
        return this.message;
    }
}
