package pers.dong.springboot.result.errorInfo;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pers.dong.springboot.result.ErrorInfoInterface;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author: dong
 * @Project: springboot
 * @Description: 统一错误码异常处理
 * @Date: Created in 9:12 AM 2018/11/13
 * @Modified By:
 */
@RestControllerAdvice
public class GlobalErrorInfoHandler {
    @ExceptionHandler(value = GlobalErrorInfoException.class)
    public ResultBody errorHandlerOverJson(HttpServletRequest request,
                                           GlobalErrorInfoException exception) {
        ErrorInfoInterface errorInfo = exception.getErrorInfo();
        ResultBody result = new ResultBody(errorInfo);
        return result;
    }
}
