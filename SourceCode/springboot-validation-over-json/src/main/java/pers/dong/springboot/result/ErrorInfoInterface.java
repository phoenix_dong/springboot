package pers.dong.springboot.result;

/**
 * @Author: dong
 * @Project: springboot
 * @Description: 错误码接口
 * @Date: Created in 9:09 AM 2018/11/13
 * @Modified By:
 */
public interface ErrorInfoInterface {
    String getCode();
    String getMessage();
}
