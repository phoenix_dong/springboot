package pers.dong.springboot.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import pers.dong.springboot.entity.City;
import pers.dong.springboot.constant.CityErrorInfoEnum;
import pers.dong.springboot.result.errorInfo.GlobalErrorInfoException;
import pers.dong.springboot.result.errorInfo.ResultBody;

/**
 * @Author: dong
 * @Project: springboot
 * @Description: 错误码案例
 * @Date: Created in 9:07 AM 2018/11/13
 * @Modified By:
 */
@RestController
@Api(description = "|find one city|")
public class ErrorJsonController {

    /**
     * 获取城市接口
     *
     * @param cityName
     * @return
     * @throws GlobalErrorInfoException
     */
    @RequestMapping(value = "/api/city", method = RequestMethod.GET)
    @ApiOperation(value = "|bb|")
    //@GetMapping("")
    public ResultBody findOneCity(@RequestParam("cityName") String cityName) throws GlobalErrorInfoException {
        // 入参为空
        if (StringUtils.isEmpty(cityName)) {
            throw new GlobalErrorInfoException(CityErrorInfoEnum.PARAMS_NO_COMPLETE);
        }
        return new ResultBody(new City(1L,2L,"温岭","是我的故乡"));
    }

}
