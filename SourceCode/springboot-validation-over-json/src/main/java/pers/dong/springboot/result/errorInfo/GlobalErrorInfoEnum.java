package pers.dong.springboot.result.errorInfo;

import pers.dong.springboot.result.ErrorInfoInterface;

/**
 * @Author: dong
 * @Project: springboot
 * @Description: 应用系统级别的错误码
 * @Date: Created in 9:10 AM 2018/11/13
 * @Modified By:
 */
public enum GlobalErrorInfoEnum implements ErrorInfoInterface {
    SUCCESS("0", "success"),
    NOT_FOUND("-1", "service not found");

    private String code;

    private String message;

    GlobalErrorInfoEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getCode(){
        return this.code;
    }

    @Override
    public String getMessage(){
        return this.message;
    }
}
